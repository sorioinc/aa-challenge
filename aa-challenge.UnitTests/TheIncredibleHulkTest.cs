﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.UnitTests
{
    [TestFixture]
    public class TheIncredibleHulkTests
    {
        [Test]
        public void ShouldBeEqualTo()
        {
            IAlgorithm a = new TheIncredibleHulk();

            var input = new string[] { "gLeDsY", "gUm", "bIrD", "lEkOpS", "oAnSoL", "ThIsIsHaRd", "oLaPeV", "sHeRgA", "FeMuR", "sUcKyJ" };

            string expectedResult = a.Process(input, 0);
            Assert.That(expectedResult, Is.EqualTo("WWdMRGVzKlRoc0lzSUhSYWQqc2NVS0p5KkxvUGFWZSpsa0VwT1MqZ21VKkZNZVJ1KmJySUQqQXNIUmVnKkFvblNMbw=="));
        }
    }
}
