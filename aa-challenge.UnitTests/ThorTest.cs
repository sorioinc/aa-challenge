﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.UnitTests
{
    [TestFixture]
    public class ThorTest
    {
        [Test]
        public void ShouldBeEqualTo()
        {
            IAlgorithm a = new Thor();

            var input = new string[]{"lEkOpS","sHeRgA","fRiGhTeNiNg","yJyLtU","aFtEr","bEaM","oOpTyN","DoWnRiVeR","BaT","bIrD"};

            string expectedResult = a.Process(input, 21);
            Assert.That(expectedResult, Is.EqualTo("MjFmVDM0cipCNTV0KkI4OTE0NG0qQjIzM3JEKmQzNzdXbipGcjYxMEdoVDk4N24xNTk3TmcqTDI1ODRrNDE4MVBzKjY3NjUxMDk0NlB0MTc3MTFOKnIyODY1N1Y0NjM2OHIqU2g3NTAyNVJnMTIxMzkzKjE5NjQxOEozMTc4MTFsVDUxNDIyOQ=="));
        }
    }
}
