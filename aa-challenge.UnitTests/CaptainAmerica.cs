﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.UnitTests
{
    [TestFixture]
    public class CaptainAmericaTest
    {
        [Test]
        public void ShouldBeEqualTo()
        {
            IAlgorithm a = new CaptainAmerica();

            var input = new string[] { "sHaRp", "cOwHyW", "sHaRp", "sHaRp", "BlUsHiNg", "sHoTgUn", "fRiGhTeNiNg", "BlUsHiNg", "FlUsH", "AgReEaBlE" };

            string expectedResult = a.Process(input, 2584);
            Assert.That(expectedResult, Is.EqualTo("c0hUMjU4NGduNDE4MTY2c0hSNjc2NXAxMTVzSFIxMDk0NnAxMTVzSFIxNzcxMXAxMTVmUkcyODY1N2hUTjQ2MzY4Tjc1MDI1ZzExNUZsczEyMTM5M0gxMDIxOTY0MThnMzE3ODExUjUxNDIyOTgzMjA0MEIxMzQ2MjY5bDcwY3cyMTc4MzA5SFczNTI0NTc4NDlCbHM1NzAyODg3SE45MjI3NDY1Zzk5QmxzMTQ5MzAzNTJITjI0MTU3ODE3ZzY2"));
        }
    }
}
