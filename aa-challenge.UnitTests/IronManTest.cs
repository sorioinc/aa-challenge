﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.UnitTests
{
    [TestFixture]
    public class IronManTests
    {
        [Test]
        public void ShouldBeEqualTo()
        {
            IAlgorithm a = new IronMan();

            var input = new string[] { "AgReEaBlE", "eEpInG", "FeMuR", "gUm", "InTeRnAl", "sHoTgUn", "yJyLtU", "LiQuId", "oAnSoL", "BaT"};

            string expectedResult = a.Process(input, 0);
            Assert.That(expectedResult, Is.EqualTo("RWdBUkVlQmFsODVCVGE2OUVlcG5JRzY2Rk1lUnU2OWdtVTcwbklUUmVubEExMDNMUWlJdWQxMTBBb25TTG83NnNIVG9nblU2NVVKeUx5dDExNQ=="));
        }
    }
}
