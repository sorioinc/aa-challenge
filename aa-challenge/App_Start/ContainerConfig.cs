﻿using aa_challenge.Algorithms;
using aa_challenge.Algorithms.Contracts;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace aa_challenge.App_Start
{
    public class ContainerConfig : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<CollectionResolverFacility>();
            container.Register(
                Component.For<IAlgorithm>().ImplementedBy<IronMan>().LifestyleTransient(),
                Component.For<IAlgorithm>().ImplementedBy<TheIncredibleHulk>().LifestyleTransient(),
                Component.For<IAlgorithm>().ImplementedBy<Thor>().LifestyleTransient(),
                Component.For<IAlgorithm>().ImplementedBy<CaptainAmerica>().LifestyleTransient(),
                Component.For<IAlgorithmFactory>().ImplementedBy<AlgorithmFactory>(),
                Classes.FromThisAssembly().BasedOn<ApiController>().LifestyleTransient()
                );

        }
    }
}