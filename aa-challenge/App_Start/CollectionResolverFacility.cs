﻿using Castle.Core.Configuration;
using Castle.MicroKernel;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aa_challenge.App_Start
{
    public class CollectionResolverFacility : IFacility
    {
        public void Init(IKernel kernel, IConfiguration facilityConfig)
        {
            kernel.Resolver.AddSubResolver(new CollectionResolver(kernel, true));
        }

        public void Terminate()
        {
        }
    }
}