﻿using aa_challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aa_challenge.Controllers
{
    public class Hits
    {
        public string Guid { get; set; }
        public EncodedPostResponse Response { get; set; }
    }
}