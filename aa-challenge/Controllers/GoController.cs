﻿using aa_challenge.Models;
using aa_challenge.Algorithms.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace aa_challenge.Controllers
{
    public class GoController : ApiController
    {
        private IAlgorithmFactory _algorithmFactory;
        public GoController(IAlgorithmFactory algorithmFactory)
        {
            _algorithmFactory = algorithmFactory;
        }
        [HttpGet]
        public async Task<IHttpActionResult> Process() 
        {
            var hits = new List<Hits>();
            for (var i = 0; i < 20; i++)
            { 
                hits.Add(new Hits());
            }

            foreach (var hit in hits)
            {
                var r = await HitUrl(hit);
            }

            return Ok(hits);
        }

        private async Task<bool> HitUrl(Hits hit)
        {
            var url = ConfigurationManager.AppSettings["URL"];
            
            using (var c = new HttpClient())
            {
                var currentGuid = Guid.NewGuid();
                hit.Guid = currentGuid.ToString();

                var values = await c.GetAsync(string.Format("{0}{1}{2}", url, "values/", currentGuid));
                if (values.IsSuccessStatusCode)
                {
                    var toResolve = await values.Content.ReadAsAsync<ToResolve>();
                    var calculated = GetResult(toResolve.algorithm, toResolve.words, toResolve.startingFibonacciNumber);
                    c.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    var encodedPost = new EncodedPost
                    {
                        emailAddress = "oscar.soriano@outlook.com",
                        name = "Oscar Soriano",
                        repoUrl = "https://bitbucket.org/sorioinc/aa-challenge",
                        webhookUrl = "http://aa-acklenchallenge.apphb.com/Webhook",
                        encodedValue = calculated
                    };

                    var encoded = await c.PostAsJsonAsync(string.Format("{0}{1}{2}/{3}", url, "values/", currentGuid, toResolve.algorithm), encodedPost);
                    if (encoded.IsSuccessStatusCode)
                    {
                        var response = await encoded.Content.ReadAsAsync<EncodedPostResponse>();
                        hit.Response = response;
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }

        private string GetResult(string algorithmName, string[] words, int startingNumber)
        {
            var algorithm = _algorithmFactory.Create(algorithmName);
            return algorithm.Process(words, startingNumber);
        }
    }
}
