﻿using aa_challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace aa_challenge.Controllers
{
    [RoutePrefix("Webhook")]
    public class WebhookController : ApiController
    {
        [HttpPost]
        [Route("")]
        public IHttpActionResult Post(Webhook model) 
        {
            model.date = DateTime.Now;
            WebhookMessages.Messages.Add(model);
            return Ok();
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(WebhookMessages.Messages.OrderByDescending(wh => wh.date));
        }
    }
}
