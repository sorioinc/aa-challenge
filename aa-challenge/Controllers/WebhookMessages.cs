﻿using aa_challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aa_challenge.Controllers
{
    public static class WebhookMessages
    {
        public static List<Webhook> Messages { get; private set; }
        static WebhookMessages()
        {
            Messages = new List<Webhook>();
        }
    }
}