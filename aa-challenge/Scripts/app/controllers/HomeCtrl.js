﻿angular.module('app.controllers')
    .controller('HomeCtrl', [
        '$rootScope', '$state', '$scope', '$stateParams', 'goService', 'webhookService',
        function ($rootScope, $state, $scope, $stateParams, go, webhook) {
            $scope.$root.title = "AA - Challenge";

            $scope.isLoading = false;
            $scope.messages = [];

            $scope.stGrid = {
                isLoading: false,
                itemsByPage: 20,
                data: [],
                displayedData: [],
                filteredData: [],
                filterLevel: function (level) {
                    this.filteredData = this.data.filter(function (element) {
                        if (element.Level === level)
                            return true;
                        return false;
                    });
                },
                displayAll: function () {
                    this.displayedData = [].concat(this.data);
                    this.filteredData = [].concat(this.data);
                }
            };

            $scope.stWebhook = {
                isLoading: false,
                itemsByPage: 20,
                data: [],
                displayedData: [],
                filteredData: [],
                filterLevel: function (level) {
                    this.filteredData = this.data.filter(function (element) {
                        if (element.Level === level)
                            return true;
                        return false;
                    });
                },
                displayAll: function () {
                    this.displayedData = [].concat(this.data);
                    this.filteredData = [].concat(this.data);
                }
            };

            $scope.process = function () {
                $scope.isProcessing = true;
                
                go.process(function (data) {
                    $scope.stGrid.data = data;
                }, function (error) {
                    $scope.parseWebApiError(error).forEach(function (value) {
                        $scope.addMessage("error", value);
                    });
                }).$promise.finally(function () {
                    $scope.stGrid.isLoading = false;
                    $scope.stGrid.displayAll();
                    $scope.isProcessing = false;
                });
            };

            $scope.getWebhookMessages = function () {
                $scope.stWebhook.isLoading = true;
                webhook.query(function (data) {
                    $scope.stWebhook.data = data;
                }, function (error) {
                    $scope.parseWebApiError(error).forEach(function (value) {
                        $scope.addMessage("error", value);
                    });
                }).$promise.finally(function () {
                    $scope.stWebhook.isLoading = false;
                    $scope.stWebhook.displayAll();
                });
            };

            $scope.addMessage = function (type, message) {
                $scope.messages.push({ id: $scope.messages.length + 1, type: type, message: message });
            };

            $scope.removeMessage = function (id) {
                var index = -1;

                for (var i = 0; i < $scope.messages.length; i += 1) {
                    if ($scope.messages[i].id === id) {
                        index = i;
                    }
                }

                if (index > -1) {
                    $scope.messages.splice(index, 1);
                }
            };

            $scope.parseWebApiError = function (error) {
                var messages = [];
                if (error.data && error.data.ModelState) {
                    for (var key in error.data.ModelState) {
                        for (var i = 0; i < error.data.ModelState[key].length; i++) {
                            messages.push(error.data.ModelState[key][i]);
                        }
                    }
                } else if (error.data && error.data.Message) {
                    messages.push(error.data.Message);
                } else if (error.Message) {
                    messages.push(error.Message);
                } else if (error.data && typeof error.data === 'string') {
                    messages.push(error.data);
                } else {
                    messages.push("An error ocurred while calling the server.");
                }
                return messages;
            };

            $scope.getWebhookMessages();
        }]);

