﻿angular.module('app.services')
    .factory('goService', ['$resource', function ($resource) {
        return $resource('/Go/', {}, {
            update: { method: 'PUT' },
            process: {
                url: 'Go/Process',
                method: 'GET',
                isArray: true
            }
        });
    }]);