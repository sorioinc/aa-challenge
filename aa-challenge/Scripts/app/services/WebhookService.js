﻿angular.module('app.services')
    .factory('webhookService', ['$resource', function ($resource) {
        return $resource('Webhook/', {}, {
            update: { method: 'PUT' }
        });
    }]);