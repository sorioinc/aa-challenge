﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aa_challenge.Models
{
    public class ToResolve
    {
        public string[] words { get; set; }
        public int startingFibonacciNumber { get; set; }
        public string algorithm { get; set; }
    }
}