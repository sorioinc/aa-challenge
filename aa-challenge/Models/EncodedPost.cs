﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aa_challenge.Models
{
    public class EncodedPost
    {
        public string encodedValue { get; set; }
        public string emailAddress { get; set; }
        public string name { get; set; }
        public string webhookUrl { get; set; }
        public string repoUrl { get; set; }
    }
}