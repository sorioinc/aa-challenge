﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aa_challenge.Models
{
    public class EncodedPostResponse
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}