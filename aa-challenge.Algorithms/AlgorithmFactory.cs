﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.Algorithms
{
    public class AlgorithmFactory : IAlgorithmFactory
    {
        private readonly IEnumerable<IAlgorithm> _statusServiceFactories;

        public AlgorithmFactory(IEnumerable<IAlgorithm> statusServiceFactories)
        {
            _statusServiceFactories = statusServiceFactories;
        }

        public IAlgorithm Create(string service)
        {
            IAlgorithm statusServiceCreator = _statusServiceFactories.FirstOrDefault(x => x.GetType().Name.StartsWith(service));

            if (statusServiceCreator == null)
                throw new ApplicationException(string.Format("Status for service '{0}' cannot be created", service));

            return statusServiceCreator;
        }
    }
}
