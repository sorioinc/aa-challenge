﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.Algorithms
{
    public class IronMan : BaseAlgorithm, IAlgorithm
    {
        public string Process(string[] input, int startingNumber)
        {
            var ordered = Order(input);
            var obfuscated = Obfuscate(ordered);
            return Encode(obfuscated);
        }

        protected override string Separator(string[] input, int i)
        {
            return (i - 1 >= 0 ? ((int)input[i - 1][0]).ToString() : ((int)input[input.Length - 1][0]).ToString());
        }
    }
}
