﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.Algorithms
{
    public class Thor : BaseAlgorithm, IAlgorithm
    {
        private string[] _englishWords = new string[] { "drool","cats","clean","code","dogs","materials","needed","this","is",
                "hard","what","are","you","smoking","shot","gun","down","river","super","man","rule","acklen","developers","are","amazing"};
        public string Process(string[] input, int startingNumber)
        {
            var splitted = SplitEnglishWords(input);
            var ordered = Order(splitted);
            var alternated = ConsonantsAlternate(ordered);
            var fibonaccied = ReplaceWithFibonacci(alternated, startingNumber);
            return Encode(fibonaccied);
        }

        protected override string Separator(string[] input, int i)
        {
            return i + 1 < input.Length ? "*" : string.Empty;
        }

        private string[] SplitEnglishWords(string[] input)
        {
            var response = new List<string>();

            foreach(var word in input)
            {
                if (!_englishWords.Any(w => word.IndexOf(w, StringComparison.CurrentCultureIgnoreCase) > -1))
                    response.Add(word);
                else 
                {
                    foreach (var ew in _englishWords)
                    { 
                        //TODO:There's a flaw, when a word contains another like: "is" and "this", they will be accounted as two words.
                        if (word.IndexOf(ew, StringComparison.CurrentCultureIgnoreCase) > -1)
                            response.Add(word.Substring(word.IndexOf(ew, StringComparison.CurrentCultureIgnoreCase), ew.Length));
                    }
                }
            }
            return response.ToArray();
        }

        private string[] ConsonantsAlternate(string[] input)
        {
            var response = new List<string>();
            var capitalIndicator = IsUpper(input[0][0]);

            foreach (var word in input)
            {
                var responseWord = new List<char>();
                for (var i = 0; i < word.Length; i++)
                {
                    var current = word[i];
                    var isCurrentAConsonant = !_vowels.Any(v => v.Equals(current));

                    if (!isCurrentAConsonant)
                    {
                        responseWord.Add(current);
                        continue;
                    }
                    if (isCurrentAConsonant && capitalIndicator)
                    {
                        responseWord.Add(current.ToString().ToUpper()[0]);
                        capitalIndicator = !capitalIndicator;
                        continue;
                    }
                    if (isCurrentAConsonant && !capitalIndicator)
                    {
                        responseWord.Add(current.ToString().ToLower()[0]);
                        capitalIndicator = !capitalIndicator;
                        continue;
                    }
                }

                response.Add(string.Join("", responseWord));
            }
            return response.ToArray();
        }

        private bool IsUpper(char char1)
        {
            return char1.ToString() == char1.ToString().ToUpper();
        }
    }
}
