﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aa_challenge.Algorithms
{
    public class Fibonacci
    {
        public int[] Series { get; private set; }
        public Fibonacci(int startingNumber, int steps)
        {
            Generate(startingNumber, steps);  
        }

        private void Generate(int number, int steps)
        { 
            var _series = new List<int>();
            var lastResult = 0;
            var currentResult = 1;
            while (currentResult < number || steps >= 0)
            {
                var result = lastResult + currentResult;
                _series.Add(result);
                lastResult = currentResult;
                currentResult = result;

                if (currentResult > number)
                    steps--;
            }

            Series = _series.Where(n => n >= number).ToArray();
        }
    }
    
}
