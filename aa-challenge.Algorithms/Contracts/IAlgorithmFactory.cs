﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aa_challenge.Algorithms.Contracts
{
    public interface IAlgorithmFactory
    {
        IAlgorithm Create(string service);
    }
}
