﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.Algorithms
{
    public class TheIncredibleHulk : BaseAlgorithm, IAlgorithm
    {

        public string Process(string[] input, int startingNumber)
        {
            var obfuscated = Obfuscate(input);
            var ordered = OrderDescending(obfuscated);
            return Encode(ordered);
        }

        protected override string Separator(string[] input, int i)
        {
            return i + 1 < input.Length ? "*" : string.Empty;
        }
    }
}
