﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aa_challenge.Algorithms.Contracts;

namespace aa_challenge.Algorithms
{
    public class CaptainAmerica : BaseAlgorithm, IAlgorithm
    {
        
        public string Process(string[] input, int startingNumber)
        {
            var obfuscated = Obfuscate(input);
            var ordered = OrderDescending(obfuscated);
            var fibonaccied = ReplaceWithFibonacci(ordered, startingNumber);
            return Encode(fibonaccied);
        }

        protected override string Separator(string[] input, int i)
        {
            return (i - 1 >= 0 ? ((int)input[i - 1][0]).ToString() : ((int)input[input.Length - 1][0]).ToString());
        }
    }
}
