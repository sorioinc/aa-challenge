﻿using aa_challenge.Algorithms.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aa_challenge.Algorithms
{
    public abstract class BaseAlgorithm
    {
        protected char[] _vowels = new char[] { 'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U', 'y', 'Y' };
        
        protected string[] Order(string[] input)
        {
            return input.OrderBy(i => i).ToArray();
        }

        protected string[] OrderDescending(string[] input)
        {
            return input.OrderByDescending(i => i).ToArray();
        }

        protected string[] Obfuscate(string[] input)
        {
            var response = new List<string>();
            foreach (var word in input)
            {
                var responseWord = new List<char>();
                for (var i = 0; i < word.Length; i++)
                {
                    var current = word[i];
                    var isCurrentAVowel = _vowels.Any(v => v.Equals(current));
                    var isCurrentNotLastChar = i + 1 < word.Length;

                    if (!isCurrentAVowel)
                    {
                        responseWord.Add(current);
                        continue;
                    }
                    if (isCurrentAVowel && !isCurrentNotLastChar)
                    {
                        responseWord.Insert(0, current);
                        continue;
                    }

                    if (isCurrentAVowel && isCurrentNotLastChar
                        //&& ( !_vowels.Any(v => v.Equals(word[i + 1]))) || 
                        //( _vowels.Any(v => v.Equals(word[i + 1])) && DifferentCase(current, word[i + 1])
                        //)
                        )
                    {
                        responseWord.Add(word[i + 1]);
                        responseWord.Add(current);
                        i++;
                        continue;
                    }
                }

                response.Add(string.Join("", responseWord));
            }
            return response.ToArray();
        }

        protected string[] ReplaceWithFibonacci(string[] input, int startingNumber)
        {
            var numberofVowels = string.Join("", input).ToCharArray().Count(c => _vowels.Contains(c));
            var f = new Fibonacci(startingNumber, numberofVowels);
            var fSentinel = 0;
            var response = new List<string>();

            foreach (var word in input)
            {
                var responseWord = new List<string>();
                for (var i = 0; i < word.Length; i++)
                {
                    var current = word[i];
                    if( _vowels.Any(v => v.Equals(current)))
                    {
                        responseWord.Add(f.Series[fSentinel].ToString());
                        fSentinel++;
                    }
                    else
                        responseWord.Add(current.ToString());
                    
                }

                response.Add(string.Join("", responseWord));
            }
            return response.ToArray();

        }

        protected string Encode(string[] input)
        {
            var response = string.Empty;
            for (var i = 0; i < input.Length; i++)
            {
                response += input[i] + Separator(input, i);
            }

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(response));
        }

        protected virtual string Separator(string[] input, int i) 
        {
            return string.Empty;
        }
    }
}
